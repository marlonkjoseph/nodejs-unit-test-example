

var proxyquire =  require('proxyquire');
var sinon = require('sinon');
var assert = require('assert');

describe('Feature', function() {
  describe('getTemperatureInCelcius', function() {
    it('should return temperature in degrees  (using fake svc)', async function() {
      // Arrange

      // Set up fake service
      const feature = proxyquire('../../feature', {
        './service': { //module paths are relative to the tested module not the test file https://www.npmjs.com/package/proxyquire#api
            weatherSvc: {
                getTemperature: sinon.fake.returns(76), // 76F
            },
            '@noCallThru': true // Prevent call through for missing stubs https://www.npmjs.com/package/proxyquire#preventing-call-thru-to-original-dependency
        }    
       })
      const zipCode = 90210

      // Act

      const result = await feature.getTemperatureInCelcius(zipCode)

      // Assert

      assert.strictEqual(result, 24.444444444444443)
    });

    it('should return temperature in degrees (withou fake svc)', async function() {
        // Arrange
  
        // Set up fake service
        const feature = require('../../feature')
        const zipCode = 90210
  
        // Act
  
        const result = await feature.getTemperatureInCelcius(zipCode)  // Will fail with service down error.
  
        // Assert

        assert.strictEqual(result, 24.444444444444443)
          
      });
  });
});
