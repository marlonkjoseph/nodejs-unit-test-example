
const { weatherSvc } = require('./service');

const getTemperatureFromWeatherService = async (zipCode) => {
    return weatherSvc.getTemperature(zipCode);
}

const getTemperatureInCelcius = async (zipCode) => {
    const tempInF = await getTemperatureFromWeatherService(zipCode)
    const tempInC = (tempInF - 32) * 5/9; // Convert from fahrenheit to celcius
    return tempInC;
}

module.exports = {
    getTemperatureFromWeatherService,
    getTemperatureInCelcius,
}
